package com.lab1.lab1.controller;

import com.lab1.lab1.entities.Caregiver;
import com.lab1.lab1.service.CaregiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/caregiver")
@CrossOrigin(origins = "http://localhost:4200")
public class CaregiverController {
    @Autowired
    CaregiverService service;

    @GetMapping("/all")
    public List<Caregiver> getAllCaregivers() {
        List<Caregiver> list = service.getAllCaregivers();
        return list;
    }

    @GetMapping("/find/{id}")
    public Caregiver getCaregiver(@PathVariable long id) {
        return service.getCaregiverById(id);
    }

    @DeleteMapping("/delete/{id}")
    public boolean deleteCaregiver(@PathVariable long id) {
        service.deleteCaregiver(id);
        return true;
    }

    @PutMapping("/update")
    public Caregiver updateCaregiver(@RequestBody Caregiver caregiver) {
        return service.setCaregiver(caregiver);
    }

    @PostMapping("/add")
    public Caregiver createCaregiver(@RequestBody Caregiver caregiver) {
        return service.setCaregiver(caregiver);
    }
}
