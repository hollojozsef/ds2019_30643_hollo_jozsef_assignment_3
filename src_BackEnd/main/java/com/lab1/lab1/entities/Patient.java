package com.lab1.lab1.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
@Table(name="patient")
public class Patient {
    @Id
    @GeneratedValue
    private long id;
    @Column(name="name")
    private String name;
    @Column(name="birth_date")
    private String birthDate;
    @Column(name="gender")
    private String gender;
    @Column(name="address")
    private String address;
    @Column(name="medical_record")
    private String medicalRecord;
//    @ManyToOne
//    @JoinColumn(name="caregiver_id",nullable = false)
//    @JsonBackReference
//    private Caregiver caregiver;
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

//    public Caregiver getCaregiver() {
//        return caregiver;
//    }
//
//    public void setCaregiver(Caregiver caregiver) {
//        this.caregiver = caregiver;
//    }

    public Patient(String name, String birthDate, String gender, String address, String medicalRecord) {
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
    }

//    public Patient(String name, String birthDate, String gender, String address, String medicalRecord, Caregiver caregiver) {
//        this.name = name;
//        this.birthDate = birthDate;
//        this.gender = gender;
//        this.address = address;
//        this.medicalRecord = medicalRecord;
//        this.caregiver = caregiver;
//    }

    public Patient() {
    }
}
