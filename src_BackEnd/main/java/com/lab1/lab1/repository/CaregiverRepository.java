package com.lab1.lab1.repository;

import com.lab1.lab1.entities.Caregiver;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CaregiverRepository extends JpaRepository<Caregiver,Long> {
    Caregiver findById(long id);
    void deleteById(long id);
}
