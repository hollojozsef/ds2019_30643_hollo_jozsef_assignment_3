import com.yrrhelp.grpc.MedicationOuterClass;
import com.yrrhelp.grpc.MedicationOuterClass.MedicationPlan;
import com.yrrhelp.grpc.MedicationOuterClass.SendId;
import com.yrrhelp.grpc.MedicationOuterClass.SendResponse;
import com.yrrhelp.grpc.medicationGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class App {
    private  JButton button1;
    private JPanel jPannel;
    private JLabel JLabel1;
    private JLabel Jlabel2;
    private JLabel Jlabel3;
    private JLabel Time;
    private JButton ignoreButton;
    int i=0;
    public static void main(String[] args) {
        JFrame frame=new JFrame("Medication App");
        frame.setContentPane(new App().jPannel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //frame.pack();
        frame.setSize(500, 300);
        frame.setVisible(true);
    }

    public App() {
        ManagedChannel managedChannel= ManagedChannelBuilder.forAddress("localhost",9090).usePlaintext().build();
        medicationGrpc.medicationBlockingStub medicationBlockingStub = medicationGrpc.newBlockingStub(managedChannel);

        ActionListener updateClockAction = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Time.setText(new Date().toString());
            }
        };
        Timer t = new Timer(1000, updateClockAction);
        t.start();
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                SendId sendId= SendId.newBuilder().setId(1).build();
                MedicationPlan medicationPlan=medicationBlockingStub.getMedicationPlan(sendId);
                JLabel1.setText(medicationPlan.getMedications(i).getName());
                SendResponse sendResponse= SendResponse.newBuilder().setIdMedication(medicationPlan.getMedications(0).getId())
                        .setIdPatient(1).setTaken(true).build();
                Jlabel2.setText(medicationPlan.getMedications(i).getDosage());
                Jlabel3.setText(medicationPlan.getMedications(i).getSideEffect());
                medicationBlockingStub.getResponseFromPatient(sendResponse);
                i++;
                MedicationPlan.Medication medicationPlan1=medicationPlan.getMedications(i);
                String str=medicationPlan1.toString();
                BufferedWriter writer = null;

                try {
                    writer = new BufferedWriter(new FileWriter("med.txt"));
                    writer.write(str);  // do something with the file we've opened
                } catch (IOException e1) {
                    // handle the exception
                } finally {
                    try {
                        if (writer != null)
                            writer.close();
                    } catch (IOException e1) {
                        // handle the exception
                    }
                }
            }
        });
        ignoreButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                SendId sendId= SendId.newBuilder().setId(1).build();
                MedicationPlan medicationPlan=medicationBlockingStub.getMedicationPlan(sendId);
                JLabel1.setText(medicationPlan.getMedications(i).getName());
                SendResponse sendResponse= SendResponse.newBuilder().setIdMedication(medicationPlan.getMedications(0).getId())
                        .setIdPatient(1).setTaken(false).build();
                Jlabel2.setText(medicationPlan.getMedications(i).getDosage());
                Jlabel3.setText(medicationPlan.getMedications(i).getSideEffect());
                medicationBlockingStub.getResponseFromPatient(sendResponse);
                i++;
            }
        });
    }
}
